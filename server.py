#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Silence My Brother"
__copyright__ = "Copyright 2014, Wi-tech project"
__version__ = "1.8.1"
__email__ = "rizha.musthafa@wicelltechnologies.com"
__status__ = "Production"

import MySQLdb
import SocketServer
import socket
import struct
import time
from xml.etree import ElementTree


class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class
    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    __geofence_path = "/var/www/html/grid_squash_ems_gps/GEOFENCE_DATA/" #configure geofence file path

    def handle(self):
        self.looping_async(1)
    
    
    def looping_async(self, i):
        try:
            self.data = self.request.recv(1024) # hold raw data send from gps
            self.data.decode('ascii')
            self.initialize_data()
            i += 1
            num_loop_bytearr = struct.unpack("2B", struct.pack("H", i))
            num_id_bytearr = struct.unpack("4B", struct.pack("I", int(self.modemid)))
            ack = chr(250) + chr(248)

            for e in num_loop_bytearr:
                ack += chr(e)

            for f in num_id_bytearr:
                ack += chr(f)

            if (i < 121):
                self.request.sendall(ack)
                self.looping_async(i)

            self.request.close()
                
        except (socket.error, UnicodeDecodeError):
            pass
        finally:
        	self.request.close()

    def command(self):
        command = "AT$PDSR?"
        self.request.sendall(command)
        self.datacommand = self.request.recv(1024)
        print self.datacommand

    def initialize_data(self):
        self.data = self.data.strip() 
        with open('gpslog', 'a+') as logfile:
            logfile.write(self.data)
        self.querybuild()

    def data_parser_position_message(self, datasplit):
        # format data from device
        # 1000001,20140310065901,106.821952,-6.21136,2,299,57,0,2,0,0,0.000,0.000,20140310065900,0
        datakey = ['modemid', 'gpsdatetime', 'longitude', 'latitude', 'speed', 'direction', 'altitude',
                   'satelites', 'messageid', 'inputstatus', 'outputstatus', 'analoginput1', 'analoginput2',
                   'rtcdatetime', 'mileage']
        data = {}
        j = 0

        for i in datakey:
            data[i] = datasplit[j]
            j += 1

        self.modemid = data['modemid']

        return data

    def querybuild(self):
        #hold list of multiple value of raw data from gps
        data = self.data.split("\r\n");
        #hold list of dictionary raw data with key and value
        datalist = []
        #hold value to be parsing for mysql query
        values = []
        
        for rawdata in data:
            datalist.append(self.data_parser_position_message(rawdata.split(',')))

        for data in datalist:
            time = data['gpsdatetime']
            data['sqltime'] = "%s-%s-%s %s:%s:%s" % (time[0:4], time[4:6], time[6:8], time[8:10], time[10:12], time[12:14])
            data['out_of_range'] = 0 #self.setOutOfRange(data)

            values.append("('%(modemid)s','%(latitude)s','%(longitude)s','%(sqltime)s','%(speed)s',"
                "'%(direction)s','%(altitude)s','%(mileage)s', %(inputstatus)s, %(out_of_range)s)" % data)
                

        values = ', '.join(values)
        fields = "`siteid`,`lat`,`long`,`last_update`,`speed`,`direction`,`altitude`,`mileage`,`engine_status`,`out_of_range`"
        query = "INSERT INTO tblocationhistory (%s) VALUES %s" % (fields, values)
        self.modemid = data['modemid']

        self.exec_qry(query)

    def setOutOfRange(self, data):
        #for future use
        try:
            #hold kml data
            kml = ElementTree.parse(self.__geofence_path + data['modemid'] + '.kml')
            #get coordinate value and store  to list
            coordinates = kml.find('.//{http://earth.google.com/kml/2.2}coordinates').text.strip().split(' ') 
            #hold list of lat from kml
            clat = [ clat.split(',')[0] for clat in coordinates]
            #hold list of long from kml
            clong = [ clong.split(',')[0] for clong in coordinates]

            return 0;

        except IOError:
            return 0

    def exec_qry(self, qry):
        #initialize mysql connection
        db = MySQLdb.connect('localhost', 'user', '', '')
        cursor = db.cursor()
        try:
            cursor.execute(qry)
            db.commit() # initialize query
            with open('gpslog', 'a+') as logfile:
                logfile.write("\r\n%s ' --- has beed executed'\n\r" % qry)
        except:
            db.rollback()
        db.close()


if __name__ == "__main__":
    HOST, PORT = "", 12122

    # Create the server, binding to localhost on port 12122
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
